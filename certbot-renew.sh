#!/bin/sh

if [ ! -z "$CERTBOT_DOMAINS" ]
then
  export IFS=";"
  for DOMAIN in $CERTBOT_DOMAINS
  do
    certbot -n --agree-tos \
    -d "$DOMAIN" \
    -m "$CERTBOT_MAIL" \
    --expand --allow-subset-of-names \
    certonly --standalone \
    --cert-name "$DOMAIN-ecdsa" \
    --preferred-challenges http --http-01-port 8008 \
    --keep-until-expiring \
    --key-type "ecdsa" \
    --config-dir /etc/letsencrypt \
    --deploy-hook "nginx -s reload"
  done
fi
