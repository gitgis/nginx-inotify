FROM nginx:alpine
MAINTAINER Grzegorz Godlewski <gg@gitgis.com>

RUN apk add inotify-tools
RUN apk add certbot certbot-nginx
RUN apk add nginx-mod-mail

COPY nginx.run /nginx.run
COPY nginx.conf /etc/nginx/nginx.conf
RUN chmod +x /nginx.run

COPY certbot-renew.sh /etc/periodic/daily/certbot-renew.sh
RUN chmod +x /etc/periodic/daily/certbot-renew.sh

EXPOSE 80/tcp 443/tcp 110/tcp 143/tcp 465/tcp 587/tcp 993/tcp 995/tcp 25/tcp 10025/tcp 10143/tcp

VOLUME ["/etc/letsencrypt", "/etc/nginx", "/var/log/nginx"]

CMD /nginx.run
